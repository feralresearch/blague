#!/usr/bin/env node
const fs = require("fs");
const { applyConfigFile } = require("./modules/config.js");
const { makeDir, traverse, debounce } = require("./modules/util.js");
const { createIndexIfNeeded, processPage } = require("./modules/process.js");
const chokidar = require("chokidar");
const yargs = require("yargs/yargs");
const { hideBin } = require("yargs/helpers");
const argv = yargs(hideBin(process.argv)).argv;
const fse = require("fs-extra");
const { exit } = require("process");
const { version } = require("../package.json");

const verbose = argv.v;
const watch = argv.watch;
console.log(
  `\n------------------------------\nBLAGUE *MDR ${version}*: ${
    watch ? "WATCH" : ""
  } ${verbose ? "V" : ""} \n------------------------------`
);
const config = applyConfigFile(argv, `.blaguerc.json`);

const onBuildSite = () => {
  console.log("[ 🪛  ]: Copying Assets...");

  try {
    fse.copySync(
      `${config.directories.input}/assets`,
      config.directories.output
    );
  } catch (e) {
    console.log(e);
  }

  if (watch)
    console.log(`[ 🧿 ]: WATCHING: ${config.directories.input} for changes...`);
  console.log(`[ 🛠  ]: BUILDING To: ${`${config.directories.output}`}`);
  makeDir(`${config.directories.output}`, () => {
    traverse({
      src: `${config.directories.input}/pages`,
      onFile: (file) => processPage(config, file),
      onDir: (dir) => {
        let subDir = dir.split("/");
        makeDir(`${config.directories.output}/${subDir[subDir.length - 1]}`);
        createIndexIfNeeded(config, dir);
      },
      recurse: true
    });
  });
};

if (fs.existsSync(`${config.directories.output}`)) {
  console.log("[ 🪛  ]: Removing existing build dir");
  fs.rmSync(`${config.directories.output}`, {
    recursive: true,
    force: true
  });
}

if (watch) {
  const handleEvent = debounce((event) => {
    onBuildSite();
  }, 1000);
  chokidar.watch(`${config.directories.input}/`).on("all", handleEvent);
} else {
  onBuildSite();
}
